export interface Item {
	id: string;
	name: string,
	perk: string,
	tags?: string[]
}

export interface BasicItem extends Item {
	id: 'bfsword' | 'recurvebow' | 'chainvest' | 'negatroncloak' | 'needlesslylargerod' | 'tearofthegoddess' | 'giantsbelt' | 'spatula'
	partOf: string[];
}

export type BasicItemCombination = ['bfsword' | 'recurvebow' | 'chainvest' | 'negatroncloak' | 'needlesslylargerod' | 'tearofthegoddess' | 'giantsbelt' | 'spatula', 'bfsword' | 'recurvebow' | 'chainvest' | 'negatroncloak' | 'needlesslylargerod' | 'tearofthegoddess' | 'giantsbelt' | 'spatula'];
export type StringNumber = [string, number];

export interface CombinedItem extends Item {
	combinedFrom: BasicItemCombination;
}

export interface CombinedItemHighlighted extends CombinedItem {
	isPicked: boolean;
}

export interface GlobalState {
	iconSize: number;
	possibleItemsIconSize: number;
}
