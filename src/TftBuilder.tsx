import * as React from 'react';
import {Dispatch, SetStateAction, useEffect, useState} from 'react';
import './TftBuilder.css';
import SingleItemList from './basic-items/SingleItemList';
import {BasicItem, CombinedItem, CombinedItemHighlighted, Item} from './interfaces';
import ItemsPicked from './basic-items/ItemsPicked';
import ItemsPossibleToBuildList from './basic-items/ItemsPossibleToBuildList';
import {BasicItems, BasicItemsContainer, CombinedItemsContainer} from './data/data';
import BoxHeader from './basic-items/BoxHeader';

const TftBuilder: React.FC = (props) => {
	
	
	const [pickedItems, setPickedItems] = useState<BasicItem[]>([]);
	const [itemsToBuild, setItemsToBuild] = useState<CombinedItemHighlighted[]>([]);
	const [itemsOwned, setItemsOwned] = useState<CombinedItem[]>([]);
	
	const [hintsEnabled, setHintsEnabled] = useState(false);
	
	const buildItem = (item: CombinedItem) => {
		const [fromOne, fromSecond] = item.combinedFrom;
		const newPickedItems = [...pickedItems];
		const indexOne = newPickedItems.indexOf(BasicItemsContainer[fromOne]);
		newPickedItems.splice(indexOne, 1);
		const indexTwo = newPickedItems.indexOf(BasicItemsContainer[fromSecond]);
		newPickedItems.splice(indexTwo, 1);
		
		setItemsOwned([...itemsOwned, item]);
		setPickedItems(newPickedItems);
	};
	
	const deleteOwnedItem = (item: CombinedItem) => {
		const newOwnedItems = [...itemsOwned];
		const indexToDelete = newOwnedItems.indexOf(item);
		newOwnedItems.splice(indexToDelete, 1);
		
		setItemsOwned(newOwnedItems);
	};
	
	const pickItem = (item: BasicItem) => {
		const newPickedItems = [...pickedItems, item];
		setPickedItems(newPickedItems);
		console.log('picked', item);
	};
	
	const deleteItem = (index: number) => {
		const newPickedItems = [...pickedItems];
		newPickedItems.splice(index, 1);
		setPickedItems(newPickedItems);
		console.log('deleted', index);
	};
	
	const calculateItemsToBuild = () => {
		console.log('hints enabled?', hintsEnabled);
		let items: CombinedItem[];
		const pickedIds = pickedItems.map(item => item.id);
		if (pickedIds.length === 1) {
			items = _handleSingleItemSelected();
		} else {
			items = _handleMultipleItemsSelected();
		}
		let result: CombinedItemHighlighted[] = items.map(item => {
			return {...item, isPicked: false};
		});
		console.log(result);
		if (hintsEnabled) {
			result = _handleHintsAddition();
		}
		console.log(result);
		setItemsToBuild(result);
	};
	
	const _handleSingleItemSelected = () => {
		console.log('handling single');
		return pickedItems[0].partOf.map(partOfId => CombinedItemsContainer[partOfId]);
	};
	
	const _handleMultipleItemsSelected = () => {
		const pickedIds = pickedItems.map(item => item.id);
		return Object.entries(CombinedItemsContainer)
		.filter(([, item]) => {
			if (item.combinedFrom[0] === item.combinedFrom[1]) {
				return pickedIds.filter(id => id === item.combinedFrom[0]).length > 1;
			} else {
				return pickedIds.includes(item.combinedFrom[0]) && pickedIds.includes(item.combinedFrom[1]);
			}
		})
		.map(([, item]) => item);
	};
	
	const _handleHintsAddition = () => {
		const pickedIds = pickedItems.map(item => item.id);
		return Object.entries(CombinedItemsContainer)
		.filter(([, item]) => {
			return pickedIds.includes(item.combinedFrom[0]) || pickedIds.includes(item.combinedFrom[1]);
		})
		.map(([, item]) => {
			return {...item, isPicked: false};
		});
	};
	
	const _setHintsEnabled = () => {
		console.log(!hintsEnabled);
		setHintsEnabled(!hintsEnabled);
	};
	
	useEffect(() => {
		calculateItemsToBuild();
	}, [hintsEnabled, pickedItems]);
	
	return (
		<div style={{display: 'flex', flexDirection: 'column', height: '100%', flexBasis: 'max-content'}}>
			<div style={{display: 'flex', flexWrap: 'wrap'}} className='lg-row-s-col'>
				<div style={{flex: 4, position: 'relative'}} className='basic-items'>
					<SingleItemList <BasicItem> header={<BoxHeader text='Choose owned basic items'/>} items={BasicItems} pickItem={pickItem}/>
				</div>
				<div style={{flex: 8}} className='basic-items'>
					<ItemsPossibleToBuildList items={itemsToBuild} setHintsEnabled={_setHintsEnabled}
					                          hintsEnabled={hintsEnabled} clickItem={buildItem}
					                          pickedItemsIds={pickedItems.map(item => item.id)}/>
				</div>
				<div style={{flex: 4, minHeight: '100px'}} className={'basic-items'}>
					<SingleItemList <CombinedItem> header={<BoxHeader text={"Owned items"}/>} items={itemsOwned} pickItem={deleteOwnedItem}/>
				</div>
			</div>
			<div className='basic-items'>
				<ItemsPicked items={pickedItems} deleteItem={deleteItem}/>
			</div>
		</div>);
};

export default TftBuilder;
