import React from 'react';
import logo from './logo.svg';
import './App.css';
import TftBuilder from './TftBuilder';
import ItemsPicked from './basic-items/ItemsPicked';

const App: React.FC = () => {
	return (
		<div className="App">
			<div className="App-container">
				<TftBuilder/>
			</div>
		</div>
	);
};

export default App;
