import React, { useGlobal, useState } from 'reactn'
import {GlobalState} from '../interfaces';

interface ItemIconProps {
	itemId: string;
	cssClass?: string;
	iconSizeRetriever?: (state: GlobalState) => number;
}

const ItemIcon = (props: ItemIconProps) => {
	const iconSizeRetriever = props.iconSizeRetriever ? props.iconSizeRetriever : (state: GlobalState) => state.iconSize;
	const [globalState] = useGlobal<GlobalState>();
	const [iconSize] = useState(iconSizeRetriever(globalState));
	console.log(iconSize);
	
	return (
		<img src={process.env.PUBLIC_URL + `/icons${iconSize === 64 ? '' : '-'+iconSize}/` + props.itemId + '.png'} alt={'1st part'}
		     className={`item-icon${iconSize === 64 ? '' : '-'+iconSize} ${props.cssClass ? props.cssClass : ''}`}/>
	)
};

export default ItemIcon;
