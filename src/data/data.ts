import {BasicItem, CombinedItem} from '../interfaces';

export const BasicItems: BasicItem[] = [
	{
		id: 'bfsword',
		name: 'B.F. Sword',
		perk: '+ 20 Attack Damage',
		partOf: ['infinityedge','hextechgunblade','spearofshojin','swordofthedivine','guardianangel','thebloodthirster', 'zekesherald', 'youmuusghostblade']
	},
	{
		id: 'chainvest',
		name: 'Chain Vest',
		perk: '+ 20 Armor',
		partOf: ['guardianangel', 'phantomdancer', 'locketoftheironsolari', 'frozenheart', 'thornmail', 'redbuff', 'knightsvow', 'swordbreaker']
	},
	{
		id: 'giantsbelt',
		name: 'Giant\'s Belt',
		perk: '+ 200 Health',
		partOf: ['zekesherald', 'titanichydra', 'morellonomicon', 'redemption', 'redbuff', 'zephyr', 'warmogsarmor', 'frozenmallet']
	},
	{
		id: 'needlesslylargerod',
		name: 'Needlesly Large Rod',
		perk: '+ 20% Spell Damage',
		partOf: ['morellonomicon', 'locketoftheironsolari', 'ludensecho', 'rabadonsdeathcap', 'guinsoosrageblade', 'hextechgunblade', 'ionicspark', 'youandme']
	},
	{
		id: 'negatroncloak',
		name: 'Negatron Cloak',
		perk: '+ 20 Magic Resist',
		partOf: ['thebloodthirster', 'cursedblade', 'ionicspark', 'hush', 'swordbreaker', 'zephyr', 'runaanshurricane', 'dragonsclaw']
	},
	{
		id: 'recurvebow',
		name: 'Recurve Bow',
		perk: '+ 20% Attack Speed',
		partOf: ['bladeoftheruinedking', 'titanichydra', 'cursedblade', 'phantomdancer', 'statikkshiv', 'guinsoosrageblade', 'rapidfirecannon', 'swordofthedivine']
	},
	{
		id: 'tearofthegoddess',
		name: 'Tear of the Goddess',
		perk: '+ 20 Starting Mana',
		partOf: ['spearofshojin', 'statikkshiv', 'ludensecho', 'seraphsembrace', 'frozenheart', 'hush', 'redemption', 'darkin']
	},
	{
		id: 'spatula',
		name: 'Spatula',
		perk: 'Spatula',
		partOf: ['youmuusghostblade', 'bladeoftheruinedking', 'youandme', 'darkin', 'knightsvow', 'runaanshurricane', 'frozenmallet', 'forceofnature']
	}
];

export const BasicItemsContainer: { [key: string]: BasicItem } = {
	'bfsword': BasicItems[0],
	'chainvest': BasicItems[1],
	'giantsbelt': BasicItems[2],
	'needlesslylargerod': BasicItems[3],
	'negatroncloak': BasicItems[4],
	'recurvebow': BasicItems[5],
	'tearofthegoddess': BasicItems[6],
	'spatula': BasicItems[7]
};

export const CombinedItemsContainer: { [key: string]: CombinedItem } = {
	'infinityedge': {
		id: 'infinityedge',
		name: 'Infinity Edge',
		perk: 'Critical Strikes deal + 100% damage',
		combinedFrom: ['bfsword', 'bfsword']
	}, 'hextechgunblade': {
		id: 'hextechgunblade',
		name: 'Hextech Gunblade',
		perk: 'Heal for 25% of all damage dealt',
		combinedFrom: ['bfsword', 'needlesslylargerod']
	}, 'spearofshojin': {
		id: 'spearofshojin',
		name: 'Spear of Shojin',
		perk: 'After casting an ability, wearer gains 15% of its max mana per attack',
		combinedFrom: ['bfsword', 'tearofthegoddess']
	}, 'swordofthedivine': {
		id: 'swordofthedivine',
		name: 'Sword of the Divine',
		perk: 'Each second, the wearer has a 5% chance to gain 100% Critical Strike until the end of the combat',
		combinedFrom: ['bfsword', 'recurvebow']
	}, 'guardianangel': {
		id: 'guardianangel',
		name: 'Guardian Angel',
		perk: 'Revive with 500 HP',
		combinedFrom: ['bfsword', 'chainvest']
	}, 'thebloodthirster': {
		id: 'thebloodthirster',
		name: 'Bloodthirster',
		perk: '50% lifesteal',
		combinedFrom: ['bfsword', 'negatroncloak']
	}, 'zekesherald': {
		id: 'zekesherald',
		name: 'Zeke\'s Herald',
		perk: 'Allies around you on combat gain 10% Attack speed',
		combinedFrom: ['bfsword', 'giantsbelt']
	}, 'youmuusghostblade': {
		id: 'youmuusghostblade',
		name: 'Youmuu\'s Ghostblade',
		perk: 'You are an Assassin',
		combinedFrom: ['bfsword', 'spatula']
	}, 'rapidfirecannon': {
		id: 'rapidfirecannon',
		name: 'Rapid Firecannon',
		perk: 'Double your attack range. Attacks cannot miss.',
		combinedFrom: ['recurvebow', 'recurvebow']
	}, 'guinsoosrageblade': {
		id: 'guinsoosrageblade',
		name: 'Guinsoo\'s Rageblade',
		perk: 'Gain 5% stacking AS on hit. Stacks infinitely.',
		combinedFrom: ['recurvebow', 'needlesslylargerod']
	}, 'statikkshiv': {
		id: 'statikkshiv',
		name: 'Stattik Shiv',
		perk: 'Every third attack splashes 100 magic damage.',
		combinedFrom: ['recurvebow', 'tearofthegoddess']
	}, 'phantomdancer': {
		id: 'phantomdancer',
		name: 'Phantom Dancer',
		perk: 'Dodge all crits.',
		combinedFrom: ['recurvebow', 'chainvest']
	}, 'cursedblade': {
		id: 'cursedblade',
		name: 'Cursed Blade',
		perk: 'Chance to shrink on hit: removes 1 star.',
		combinedFrom: ['recurvebow', 'negatroncloak']
	}, 'titanichydra': {
		id: 'titanichydra',
		name: 'Titanic Hydra',
		perk: 'Attacks deal 10% of the wearer’s maximum HP as bonus splash damage.',
		combinedFrom: ['recurvebow', 'giantsbelt']
	}, 'bladeoftheruinedking': {
		id: 'bladeoftheruinedking',
		name: 'Blade of the Ruined King',
		perk: 'You are a Blademaster.',
		combinedFrom: ['recurvebow', 'spatula']
	}, 'locketoftheironsolari': {
		id: 'locketoftheironsolari',
		name: 'Locket of the Iron Solari',
		perk: 'Shield units around you for 200 Health on combat start.',
		combinedFrom: ['chainvest', 'needlesslylargerod']
	}, 'frozenheart': {
		id: 'frozenheart',
		name: 'Frozen Heart',
		perk: 'Adjacent enemies attack 20% slower.',
		combinedFrom: ['chainvest', 'tearofthegoddess']
	}, 'thornmail': {
		id: 'thornmail',
		name: 'Thornmail',
		perk: 'Reflect 35% of attack damage taken.',
		combinedFrom: ['chainvest', 'chainvest']
	}, 'swordbreaker': {
		id: 'swordbreaker',
		name: 'Sword Breaker',
		perk: 'Attacks have a chance to disarm.',
		combinedFrom: ['chainvest', 'negatroncloak']
	}, 'redbuff': {
		id: 'redbuff',
		name: 'Red Buff',
		perk: 'Attacks burn for 2.5 percent max Health. Burned units cannot heal.',
		combinedFrom: ['chainvest', 'giantsbelt']
	}, 'knightsvow': {
		id: 'knightsvow',
		name: 'Knight\'s Vow',
		perk: 'You are a Knight.',
		combinedFrom: ['chainvest', 'spatula']
	}, 'ionicspark': {
		id: 'ionicspark',
		name: 'Ionic Spark',
		perk: 'Whatever an enemy casts, zap them for 200 damage.',
		combinedFrom: ['negatroncloak', 'needlesslylargerod']
	}, 'hush': {
		id: 'hush',
		name: 'Hush',
		perk: 'High chance to silence on hit.',
		combinedFrom: ['negatroncloak', 'tearofthegoddess']
	}, 'dragonsclaw': {
		id: 'dragonsclaw',
		name: 'Dragon\'s Claw',
		perk: '83% resistance to magic damage.',
		combinedFrom: ['negatroncloak', 'negatroncloak']
	}, 'zephyr': {
		id: 'zephyr',
		name: 'Zephyr',
		perk: 'On combat start, banish an enemy for 5 seconds.',
		combinedFrom: ['negatroncloak', 'giantsbelt']
	}, 'runaanshurricane': {
		id: 'runaanshurricane',
		name: 'Runaan\'s Hurricane',
		perk: 'Attack 2 extra targets on attack. Extra attacks deal 50% damage.',
		combinedFrom: ['negatroncloak', 'spatula']
	}, 'rabadonsdeathcap': {
		id: 'rabadonsdeathcap',
		name: 'Rabadon\'s Deathcap',
		perk: '+50% AP',
		combinedFrom: ['needlesslylargerod', 'needlesslylargerod']
	}, 'ludensecho': {
		id: 'ludensecho',
		name: 'Luden\'s Echo',
		perk: 'On spellhit, deal 200 splash damage.',
		combinedFrom: ['needlesslylargerod', 'tearofthegoddess']
	}, 'morellonomicon': {
		id: 'morellonomicon',
		name: 'Morellonomicon',
		perk: 'Spells deal burn damage and prevent all healing.',
		combinedFrom: ['needlesslylargerod', 'giantsbelt']
	}, 'youandme': {
		id: 'youandme',
		name: 'You and me',
		perk: 'You are a Sorcerer.',
		combinedFrom: ['needlesslylargerod', 'spatula']
	}, 'seraphsembrace': {
		id: 'seraphsembrace',
		name: 'Seraph\'s Embrace',
		perk: 'Regain 20 mana after each spell cast.',
		combinedFrom: ['tearofthegoddess', 'tearofthegoddess']
	}, 'redemption': {
		id: 'redemption',
		name: 'Redemption',
		perk: 'On death, heal nearby allies for 1000 Health.',
		combinedFrom: ['tearofthegoddess', 'giantsbelt']
	}, 'darkin' : {
		id: 'darkin',
		name: 'Darkin',
		perk: 'You are a Demon.',
		combinedFrom: ['tearofthegoddess', 'spatula']
	}, 'warmogsarmor': {
		id: 'warmogsarmor',
		name: 'Warmog\'s Armor',
		perk: 'Regenerate 5% health per second.',
		combinedFrom: ['giantsbelt', 'giantsbelt']
	}, 'frozenmallet': {
		id: 'frozenmallet',
		name: 'Frozen Mallet',
		perk: 'You are Glacial.',
		combinedFrom: ['giantsbelt', 'spatula']
	}, 'forceofnature': {
		id: 'forceofnature',
		name: 'Force of Nature',
		perk: 'Team size +1',
		combinedFrom: ['spatula', 'spatula']
	}
};
