import * as React from 'react';
import {ReactNode} from 'react';

interface BoxHeaderProps {
	text: string;
	children?: ReactNode;
}

const BoxHeader = (props: BoxHeaderProps) => {
	return (
		<div style={{
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
			padding: '7px',
			backgroundColor: 'rgb(95, 45, 5)'
		}}
		     className='first-item dark-color box-header'>
			<span className='item-title'>{props.text}</span>
			<div style={{marginLeft: 'auto', display: 'flex'}}>
				{props.children}
			</div>
		</div>
	);
};

export default BoxHeader;
