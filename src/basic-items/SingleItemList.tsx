import * as React from 'react';
import SingleItemDisplay from './SingleItemDisplay';
import {Item} from '../interfaces';

interface SingleItemListProps<T extends Item> {
	header: React.ReactNode;
	items: T[];
	pickItem: (item: T) => void;
}

const SingleItemList = <T extends Item>(props: SingleItemListProps<T>) => {
	
	const calcItemPosition = (index: number) => index === props.items.length - 1 ? 'last' : '';
	
	const renderItem = (item: T, index: number) => {
		return <SingleItemDisplay key={index} item={item} position={calcItemPosition(index)} pickItem={props.pickItem}/>;
	};
	
	return (
		<div>
			{props.header}
			{props.items.map((item, idx) => renderItem(item, idx))}
		</div>
	);
};

export default SingleItemList;
