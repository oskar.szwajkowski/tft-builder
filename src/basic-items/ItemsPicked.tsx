import * as React from 'react';
import {BasicItem} from '../interfaces';
import ItemPickedDisplay from './ItemPickedDisplay';
import BoxHeader from './BoxHeader';

interface ItemsPickedProps {
	items: BasicItem[];
	deleteItem: (index: number) => void;
}

const ItemsPicked = (props: ItemsPickedProps) => {
	const renderItem = (item: BasicItem, index: number) => {
		return <ItemPickedDisplay key={index} item={item} index={index} deleteItem={props.deleteItem}/>;
	};
	
	const renderMessageIfEmpty = () => {
		if (props.items.length === 0) {
			return <p className='dark-color item-title' style={{textAlign: 'center', justifySelf: 'center'}}>No item chosen</p>;
		}
	};
	
	return (
		<div style={{display: 'flex', flexDirection: 'column'}}>
			<BoxHeader text={'Owned basic items'}/>
			{renderMessageIfEmpty()}
			<div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', padding: '3px'}}>
				{props.items.map((item, index) => renderItem(item, index))}
			</div>
		</div>
	);
};

export default ItemsPicked;
