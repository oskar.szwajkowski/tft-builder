import * as React from 'react';
import {Item} from '../interfaces';
import './BasicItemDisplay.css';

interface SingleItemDisplayProps<T> {
	item: T;
	position?: string;
	pickItem: (item: T) => void;
}

const SingleItemDisplay = <T extends Item>(props: SingleItemDisplayProps<T>) => {
	
	const getCssClassBasedOnPosition = () => {
		if (props.position) {
			if (props.position === 'first') {
				return 'first-item';
			} else {
				return 'last-item';
			}
		} else {
			return '';
		}
	};
	
	return (
		<div style={{display: 'flex', cursor: 'pointer'}}
		     className={'item-container dark-color hvr-fade ' + getCssClassBasedOnPosition()}
		     onClick={() => props.pickItem(props.item)}>
			<div style={{display: 'flex', padding: '5px'}}>
				<img src={process.env.PUBLIC_URL + '/icons/' + props.item.id + '.png'} alt={'Item icon'}
				     className='item-icon'/>
				<div style={{display: 'flex', flexDirection: 'column', paddingLeft: '10px'}}>
					<p className='no-margin item-title'>{props.item.name}</p>
					<p className='no-margin item-description'>{props.item.perk}</p>
				</div>
			</div>
		</div>
	);
};

export default SingleItemDisplay;
