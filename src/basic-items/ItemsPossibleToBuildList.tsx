import * as React from 'react';
import {CombinedItem, CombinedItemHighlighted} from '../interfaces';
import ItemPossibleToBuildDisplay from './ItemPossibleToBuildDisplay';
import BoxHeader from './BoxHeader';
import {Switch} from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import {orange} from '@material-ui/core/colors';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import {Info} from '@material-ui/icons';

interface ItemsPossibleToBuildListProps {
	pickedItemsIds: string[];
	items: CombinedItemHighlighted[];
	setHintsEnabled: () => void;
	hintsEnabled: boolean;
	clickItem: (item: CombinedItem) => void;
}

const ItemsPossibleToBuildList = (props: ItemsPossibleToBuildListProps) => {
	const renderItem = (item: CombinedItemHighlighted, index: number) => {
		return <ItemPossibleToBuildDisplay key={index} item={item} highlightEnabled={props.hintsEnabled}
		                                   isFirstSecondPicked={_isFirstSecondPicked(item)}
		                                   clickItem={props.clickItem}/>;
	};
	
	const _isFirstSecondPicked: (item: CombinedItemHighlighted) => [boolean, boolean] = (item: CombinedItemHighlighted) => {
		if (item.combinedFrom[0] === item.combinedFrom[1]) {
			const combinedFrom = item.combinedFrom[0];
			const pickedMatchCombinedFrom = props.pickedItemsIds.filter(item => item === combinedFrom);
			return pickedMatchCombinedFrom.length > 1 ? [true, true] : pickedMatchCombinedFrom.length === 1 ? [true, false] : [false, false];
		} else {
			return [props.pickedItemsIds.includes(item.combinedFrom[0]), props.pickedItemsIds.includes(item.combinedFrom[1])];
		}
	};
	
	const _sortTwoItems = (itemOne: CombinedItemHighlighted, itemTwo: CombinedItemHighlighted) => {
		const firstDependenciesPicked = _isFirstSecondPicked(itemOne);
		const secondDependenciesPicked = _isFirstSecondPicked(itemTwo);
		console.log(itemOne.name, firstDependenciesPicked, itemTwo.name, secondDependenciesPicked, ((firstDependenciesPicked[0] && firstDependenciesPicked[1]) > (secondDependenciesPicked[0] && secondDependenciesPicked[1])) ? 1 : 0);
		return ((firstDependenciesPicked[0] && firstDependenciesPicked[1]) > (secondDependenciesPicked[0] && secondDependenciesPicked[1])) ? -1 :
			((firstDependenciesPicked[0] && firstDependenciesPicked[1]) < (secondDependenciesPicked[0] && secondDependenciesPicked[1])) ? 1 : 0;
	};
	
	const renderMessageIfEmpty = () => {
		if (props.items.length === 0) {
			return <p className='dark-color item-title' style={{textAlign: 'center'}}>Can't build any item :(</p>;
		}
	};
	
	const HintsModeSwitch = withStyles({
		switchBase: {
			color: '#d27828',
			'&$checked': {
				color: orange[600],
			},
			'&$checked + $track': {
				backgroundColor: orange[200],
			}
		},
		checked: {},
		track: {},
	})(Switch);
	
	return (
		<div>
			<BoxHeader text='Items possible to build'>
				<Tooltip style={{paddingTop: '0px'}} placement={'top'}
				         title={
					         <div className={'nice-font'} style={{fontSize: 'x-large', lineHeight: '27px'}}>
						         Hints will show picked items combination will all other items
						         (useful when trying to make valuable targets on fountain).
						         <br/><br/>
						         Hints are automatically enabled when single item is selected.
					         </div>}>
					<IconButton>
						<Info/>
					</IconButton>
				</Tooltip>
				<span className="item-title" style={{marginRight: '8px'}}>Hints enabled</span>
				<div style={{marginTop: '-10px'}}>
					<HintsModeSwitch color={'primary'} onChange={props.setHintsEnabled}
					                 defaultChecked={props.hintsEnabled}/>
				</div>
			</BoxHeader>
			{renderMessageIfEmpty()}
			<div style={{maxHeight: '592px', overflowY: 'auto', marginLeft: '5px', marginBottom: '5px'}}>
				{props.items.sort(_sortTwoItems).map((item, index) => renderItem(item, index))}
			</div>
		</div>
	);
};

export default ItemsPossibleToBuildList;
