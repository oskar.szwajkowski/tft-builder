import React from 'reactn';
import {CombinedItem} from '../interfaces';
import BoxHeader from './BoxHeader';

interface BuildItemsProps {
	items: CombinedItem[];
}

const BuiltItems = (props: BuildItemsProps) => {
	return (
		<div>
			<BoxHeader text={"Owned items"}/>
		</div>
	);
};

export default BuiltItems;
