import React, {useGlobal} from 'reactn';
import {CombinedItem, CombinedItemHighlighted, GlobalState} from '../interfaces';
import ItemIcon from '../atomic-components/ItemIcon';
import {CSSProperties} from 'react';

interface ItemPossibleToBuildDisplay {
	item: CombinedItemHighlighted;
	highlightEnabled: boolean;
	isFirstSecondPicked: [boolean, boolean];
	clickItem: (item: CombinedItem) => void;
}

const IconSizeExtractor = (state: GlobalState) => state.possibleItemsIconSize;

const ItemPossibleToBuildDisplay = (props: ItemPossibleToBuildDisplay) => {
	
	const [globalState] = useGlobal<GlobalState>();
	const signStyle: CSSProperties = {
		fontSize: '48px', display: 'flex', alignItems: 'center', maxHeight: globalState.possibleItemsIconSize
	};
	
	return (
		<div style={{display: 'flex', cursor: 'pointer', flexDirection: 'column', boxSizing: 'border-box'}}
		     className={'item-container dark-color'} onClick={() => props.clickItem(props.item)}>
			<div style={{display: 'flex', padding: '5px', alignItems: 'center'}}>
				<div style={{
					display: 'flex',
					flexDirection: 'row',
					alignContent: 'space-between',
					alignItems: 'center'
				}}>
					<ItemIcon itemId={props.item.combinedFrom[0]}
					          cssClass={props.highlightEnabled ? props.isFirstSecondPicked[0] ? 'green-highlight' : 'red-highlight' : ''}
					          iconSizeRetriever={IconSizeExtractor}/>
					<span style={signStyle}> + </span>
					<ItemIcon itemId={props.item.combinedFrom[1]}
					          cssClass={props.highlightEnabled ? props.isFirstSecondPicked[1] ? 'green-highlight' : 'red-highlight' : ''}
					          iconSizeRetriever={IconSizeExtractor}/>
					<span style={signStyle}>=</span>
				</div>
				<ItemIcon itemId={props.item.id} iconSizeRetriever={IconSizeExtractor}/>
				<div style={{display: 'flex', flexDirection: 'column', paddingLeft: '10px', alignSelf: 'flex-start'}}>
					<p className='no-margin item-title'>{props.item.name}</p>
					<p className='no-margin item-description'>{props.item.perk}</p>
				</div>
			</div>
		</div>
	);
};

export default ItemPossibleToBuildDisplay;
