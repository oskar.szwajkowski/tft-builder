import * as React from 'react';
import {BasicItem} from '../interfaces';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import './BasicItemDisplay.css';

interface ItemPickedDisplay {
	index: number;
	item: BasicItem;
	deleteItem: (index: number) => void;
}

const ItemPickedDisplay = (props: ItemPickedDisplay) => {
	return (
		<div style={{display: 'flex', width: 'fit-content', minWidth: '200px'}} className='dark-color  item-container item-chosen hvr-fade'
		     onClick={() => props.deleteItem(props.index)}>
			<div style={{display: 'flex'}}>
				<img src={process.env.PUBLIC_URL + '/icons/' + props.item.id + '.png'} alt={'Item icon'}
				     className='item-icon'/>
				<div style={{display: 'flex', flexDirection: 'column', paddingLeft: '10px'}}>
					<p className='no-margin item-title'>{props.item.name}</p>
				</div>
			</div>
			{/*<Button variant="contained" color="secondary" href={''}*/}
			{/*        style={{maxHeight: '35px', marginRight: '5px', alignSelf: 'center'}}*/}
			{/*        onClick={() => props.deleteItem(props.index)}>*/}
			{/*	<DeleteIcon/>*/}
			{/*</Button>*/}
		</div>
	);
};

export default ItemPickedDisplay;
